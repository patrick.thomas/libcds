#include <stdio.h>
#include <time.h>
#include "cdstime.c"

typedef struct gps_test_point
{
    struct tm utc;
    int       gps_s;
    int       is_transition;
} gps_test_point;

gps_test_point test_points[ 128 ];
int            num_test_points = 0;

void
print_testpoint( const gps_test_point* tp )
{
    printf( "%c %04d-%02d-%02d %02d:%02d:%02d gps=%d\n",
            tp->is_transition ? '*' : ' ',
            tp->utc.tm_year + 1900,
            tp->utc.tm_mon + 1,
            tp->utc.tm_mday,
            tp->utc.tm_hour,
            tp->utc.tm_min,
            tp->utc.tm_sec,
            tp->gps_s );
}

int
read_testpoint( const char* line )
{
    int             dummy;
    gps_test_point* t = test_points + num_test_points;
    int             num_scanned = sscanf( line,
                              ",%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
                              &dummy,
                              &dummy,
                              &t->utc.tm_year,
                              &dummy,
                              &t->utc.tm_mon,
                              &t->utc.tm_mday,
                              &t->utc.tm_hour,
                              &t->utc.tm_min,
                              &t->utc.tm_sec,
                              &t->utc.tm_yday,
                              &t->gps_s,
                              &t->is_transition );
    if ( num_scanned == 12 )
    {
        t->utc.tm_mon--;
        t->utc.tm_year -= 1900;
        //print_testpoint(t);
        num_test_points++;
        return 1;
    }
    else
    {
        return 0;
    }
}

void
read_file( const char* fname )
{
    FILE* f = fopen( fname, "rt" );
    if ( NULL == f )
    {
        return;
    }
    char buff[ 256 ];
    while ( !feof( f ) )
    {
        fgets( buff, sizeof( buff ), f );
        read_testpoint( buff );
    }
    fclose( f );
}

int
test_test_point( const gps_test_point* tp )
{

    int     all_passed = 1;
    int     utc_to_gps_passed = 0;
    gps_s_t gps_s = GPSfromUTC_s( &tp->utc );
    if ( gps_s == tp->gps_s )
    {
        utc_to_gps_passed = 1;
    }
    else
    {
        all_passed = 0;
    }

    int      gps_ns_passed = 0;
    gps_ns_t gps_ns = GPSfromUTC_ns( &tp->utc );
    if ( gps_ns == tp->gps_s * NSECS_PER_SEC )
    {
        gps_ns_passed = 1;
    }
    else
    {
        all_passed = 0;
    }

    int       gps_to_utc_passed = 0;
    struct tm utc;
    UTCfromGPS_s( tp->gps_s, &utc );

    struct tm working_tm;

    working_tm = tp->utc;
    time_t expected_utc = mktime( &working_tm );
    working_tm = utc;
    time_t got_utc = mktime( &working_tm );

    if ( expected_utc == got_utc )
    {
        gps_to_utc_passed = 1;
    }
    else
    {
        all_passed = 0;
    }

    print_testpoint( tp );
    char buff1[ 32 ];
    char buff2[ 32 ];
    asctime_r( &tp->utc, buff1 );
    asctime_r( &utc, buff2 );
    buff1[ 24 ] = 0;
    buff2[ 24 ] = 0;
    printf( "\tUTC to GPS (s) expected %d got %lu %s\n",
            tp->gps_s,
            gps_s,
            utc_to_gps_passed ? "PASSED" : "FAILED" );
    printf( "\tUTC to GPS (ns) expected %lu got %lu %s\n",
            (uint64_t)tp->gps_s * NSECS_PER_SEC,
            gps_ns,
            gps_ns_passed ? "PASSED" : "FAILED" );
    printf( "\tGPS (s) to UTC expected %s got %s %s\n",
            buff1,
            buff2,
            gps_to_utc_passed ? "PASSED" : "FAILED" );

    if ( tp->is_transition )
    {
        int       gps_to_utc_leap_passed = 0;
        struct tm utc;
        UTCfromGPS_s( tp->gps_s + 1, &utc );

        working_tm = tp->utc;
        time_t expected_utc = mktime( &working_tm );
        working_tm = utc;
        time_t got_utc = mktime( &working_tm );

        if ( expected_utc == got_utc )
        {
            gps_to_utc_leap_passed = 1;
        }
        else
        {
            all_passed = 0;
        }

        char buff1[ 32 ];
        char buff2[ 32 ];
        asctime_r( &tp->utc, buff1 );
        asctime_r( &utc, buff2 );
        buff1[ 24 ] = 0;
        buff2[ 24 ] = 0;
        printf( "\tGPS (s) to UTC across leap expected %s got %s %s\n",
                buff1,
                buff2,
                gps_to_utc_leap_passed ? "PASSED" : "FAILED" );
    }

    return all_passed;
}

int
tables_equal( leap_table_t* t1, leap_table_t* t2 )
{

    if ( t1->leap_seconds == NULL || t2->leap_seconds == NULL )
    {
        printf( "\t\tTried to compare a NULL table." );
        return 0;
    }
    if ( t1->leap_seconds->len != t2->leap_seconds->len )
    {
        printf( "\t\tLengths weren't equal. %d != %d.\n",
                t1->leap_seconds->len,
                t2->leap_seconds->len );
        return 0;
    }
    for ( int i = 0; i < t1->leap_seconds->len; ++i )
    {
        leap_t* t1_data = (leap_t*)t1->leap_seconds->data;
        leap_t* t2_data = (leap_t*)t2->leap_seconds->data;

        if ( t1_data[ i ].transition != t2_data[ i ].transition )
        {
            printf( "\t\tTransitions differed on item %d: %lu != %lu\n",
                    i,
                    t1_data[ i ].transition,
                    t2_data[ i ].transition );
            return 0;
        }

        if ( t1_data[ i ].change != t2_data[ i ].change )
        {
            printf( "\t\tTotal leaps differed on item %d: %d != %d\n",
                    i,
                    t1_data[ i ].change,
                    t2_data[ i ].change );
            return 0;
        }
    }
    return 1;
}

/**
 * Test whether all tables load ok.
 * @return
 */
int
table_tests( )
{
    printf( "Comparing leap second tables\n" );
    int passed = 1;

    leap_table_t* iers = read_IERS_leap_seconds_linux( );
    leap_table_t* nist = read_NIST_leap_seconds_linux( );
    leap_table_t* fallback = get_fallback_leaps( );
    printf( "\tComparing IERS table to NIST table\n" );
    int result = tables_equal( iers, nist );
    passed = passed && result;
    printf( "\t\tResult = %s\n", result ? "PASSED" : "FAILED" );
    printf( "\tComparing IERS table to fallback table\n" );
    result = tables_equal( iers, fallback );
    passed = passed && result;
    printf( "\t\tResult = %s\n", result ? "PASSED" : "FAILED" );
    printf( "Leap second table comparison: %s\n",
            passed ? "PASSED" : "FAILED" );
}

int
other_funcs( )
{
    int passed = 1;

    GPSnow_s( );
    GPSnow_ns( );
    utc_t utc;
    if ( NULL == UTCfromGPS_s( 0, &utc ) )
    {
        passed = 0;
        printf( "failed on UTCfromGPS\n" );
    }
    if ( 0 != GPSfromUTC_s( &utc ) )
    {
        passed = 0;
        printf( "failed on GPSfromUTC\n" );
    }
    if ( NULL == UTCfromGPS_ns( 0, &utc ) )
    {
        passed = 0;
        printf( "failed on UTCfromGPS_ns\n" );
    }

    gps_t gps;
    if ( 0 != GPS_from_nsec( 0, &gps ) )
    {
        passed = 0;
        printf( "failed on GPS_from_nsec\n" );
    }

    return passed;
}

int
run_tests( )
{
    int all_passed = 1;
    for ( int i = 0; i < num_test_points; ++i )
    {
        all_passed = test_test_point( test_points + i ) && all_passed;
    }
    all_passed = table_tests( ) && all_passed;
    all_passed = other_funcs( ) && all_passed;
    return all_passed;
}

int
main( void )
{
    gps_t now;
    GPSnow( &now );
    printf( "seconds=%lu\nns=%u\n", now.seconds, now.nanoseconds );
    GPSnow( &now );
    printf( "seconds=%lu\nns=%u\n", now.seconds, now.nanoseconds );
    read_file( "testpoints.csv" );
    int all_passed = 0;
    all_passed = run_tests( );
    printf( "\nOverall Result: %s\n", all_passed ? "PASSED" : "FAILED" );

    return all_passed ? 0 : 1;
}
