add_executable(timetest
        timetest.c
)

target_include_directories(timetest PUBLIC
        ${CMAKE_SOURCE_DIR}/src
        ${CMAKE_SOURCE_DIR}/include
        /usr/include/glib-2.0
        /usr/lib/x86_64-linux-gnu/glib-2.0/include
)

target_link_libraries(timetest PUBLIC
    glib-2.0
)

add_test(NAME time_convert_test
        COMMAND ${CMAKE_CURRENT_BINARY_DIR}/timetest
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)