/* Version: $Id$ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: gdsmain							*/
/*                                                         		*/
/* Module Description: Defines GDS target parameters	 		*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 03Apr98  D. Sigg    	First release		   		*/
/* 0.2	 08Apr98  M. Pratt    	removed enumeration of GDS_TARGET	*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: gdsmain.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8336  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.5.1		*/
/*	Compiler Used: sun workshop C 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_MAIN_H
#define _GDS_MAIN_H

#include <stdio.h>

/** Macro which returns a subdirectory under the GDS archive directory.
    The subdirectory path must include a preceeding '/', but not a 
    trailing one. The returned directory is 
    
    <ARCHIVE><subdir>
    
    @param subdir string describing the subdirectory path 
    @return subdirectory in the GDS archive directory
    @author DS, March 98
    @see Main and GDS_ARCHIVE
************************************************************************/
#define gdsPath( subdir ) ARCHIVE subdir

/** Macro which returns a parameter file section extended by the
    site and interferometer qualifier.
    The returned sections is 
    
    SITE_PREFIX IFO_PREFIX "-" section
    
    @param section parameter file section 
    @return qualified parameter file section
    @author DS, March 98
    @see Main and GDS_ARCHIVE
************************************************************************/
#define gdsSectionSiteIfo( section ) SITE_PREFIX IFO_PREFIX "-" section

/** Macro which returns a parameter file section extended by the
    site qualifier. The returned sections is 
    
    SITE_PREFIX "-" section
    
    @param section parameter file section 
    @return qualified parameter file section
    @author DS, March 98
    @see Main and GDS_ARCHIVE
************************************************************************/
#define gdsSectionSite( section ) SITE_PREFIX "-" section

/** Macro which returns a filename under the GDS archive directory.
    The subdirectory path must include a preceeding '/', but not a 
    trailing one. The returned filename is 
    
    <ARCHIVE><subdir>/<filename>
    
    @param subdir string describing the subdirectory path 
    @param filename string describing the short name of the file
    @return name of file in the GDS archive directory
    @author DS, March 98
    @see Main and GDS_ARCHIVE
************************************************************************/
static inline char*
gdsPathFile( const char* archive, const char* subdir, const char* filename )
{
    static char s[ 1024 ];
    snprintf( s, sizeof( s ), "%s%s/%s", archive, subdir, filename );
    return s;
}

static inline char*
gdsPathFile2( const char* archive,
              const char* subdir,
              const char* filename,
              const char* c1,
              const char* c2 )
{
    static char s[ 1024 ];
    snprintf(
        s, sizeof( s ), "%s%s/%s%s%s", archive, subdir, filename, c1, c2 );
    return s;
}

/* just use the con/de-structor attributes */
#define __init__( name )                                                       \
    static void name( void ) __attribute__( ( constructor ) )

#define __fini__( name )                                                       \
    static void name( void ) __attribute__( ( destructor ) )

/*@}*/

#endif /*_GDS_ERR_H */
