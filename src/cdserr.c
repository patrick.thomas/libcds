static char* versionId = "Version $Id$";
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: gdserr							*/
/*                                                         		*/
/* Procedure Description: displays a message on the GDS			*/
/* system consoles.							*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* Header File List: */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <signal.h>
#include <sys/time.h>
#include <unistd.h>

#include "cds/cdsutil.h"

/* definitions */

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: readParameters				*/
/*                                                         		*/
/* Procedure Description: reads console server parameters from		*/
/* file.								*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static void
readParameters( void )
{
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: writeToSocket				*/
/*                                                         		*/
/* Procedure Description: writes a message to a socket			*/
/* 									*/
/*                                                         		*/
/* Procedure Arguments: addr: server addr.; port: port #; msg: message	*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static void
writeToSocket( unsigned long addr, int port, const char* msg )
{
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: writeMessage				*/
/*                                                         		*/
/* Procedure Description: writes a message to the console server	*/
/* 									*/
/*                                                         		*/
/* Procedure Arguments: msgType: type of message; msg: message		*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static void
writeMessage( int msgType, const char* msg )
{
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsConsoleMessage				*/
/*                                                         		*/
/* Procedure Description: displays a console message on the GDS		*/
/* system consoles.							*/
/*                                                         		*/
/* Procedure Arguments: char* msg					*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
void
gdsConsoleMessage( const char* msg )
{
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsErrorMessage				*/
/*                                                         		*/
/* Procedure Description: displays an error message on the GDS		*/
/* system consoles.							*/
/*                                                         		*/
/* Procedure Arguments: char* msg					*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
void
gdsErrorMessage( const char* msg )
{
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsError    				*/
/*                                                         		*/
/* Procedure Description: displays an error message on the GDS		*/
/* system consoles using the error number.				*/
/*                                                         		*/
/* Procedure Arguments: int num, char* msg				*/
/*                                                         		*/
/* Procedure Returns: int						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int
gdsErrorEx( int num, const char* msg, const char* file, int line )
{
    return 0;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsWarningMessage				*/
/*                                                           		*/
/* Procedure Description: displays a warning message on the GDS		*/
/* system consoles.							*/
/*                                                         		*/
/* Procedure Arguments: char* msg					*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
void
gdsWarningMessage( const char* msg )
{
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsDebugMessage				*/
/*                                                         		*/
/* Procedure Description: displays a debug message on the GDS		*/
/* system consoles.							*/
/*                                                         		*/
/* Procedure Arguments: char* msg					*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
void
gdsDebugMessage( const char* msg )
{
}

void
gdsDebugMessageEx( const char* msg, const char* file, int line )
{
    printf( "%s:%i %s\n", file, line, msg );
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: waitForMessages				*/
/*                                                         		*/
/* Procedure Description: waits fro input from the stdout pipe and	*/
/* the stderr pipe. Copies messages to the system console.		*/
/*                                                         		*/
/* Procedure Arguments: int stdOut, int stdErr, int pdOut, int pdErr	*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static void
waitForMessages( int stdOut, int stdErr, int pdOut, int pdErr )
{
    fd_set port_set;
    char*  buf;
    int    num;

    /* allocate message buffer */
    buf = malloc( MAXERRMSG + 10 );
    if ( buf == NULL )
    {
        return;
    }

    /* loop and wait for input */
    while ( 1 )
    {

        /* setup for select */
        FD_ZERO( &port_set );
        if ( stdOut )
        {
            FD_SET( pdOut, &port_set );
        }
        if ( stdErr )
        {
            FD_SET( pdErr, &port_set );
        }

        /* wait for input from either pipe */
        if ( select( FD_SETSIZE, &port_set, NULL, NULL, NULL ) > 0 )
        {
            /* copies to system console and stdout */
            if ( ( stdOut ) && ( FD_ISSET( pdOut, &port_set ) ) )
            {
                num = read( pdOut, buf, MAXERRMSG );
                if ( num > 0 )
                {
                    buf[ num ] = '\0';
                    fprintf( stdout, "%s", buf );
                    gdsConsoleMessage( buf );
                }
            }
            /* copies to system console and stderr */
            if ( ( stdErr ) && ( FD_ISSET( pdErr, &port_set ) ) )
            {
                num = read( pdErr, buf, MAXERRMSG );
                if ( num > 0 )
                {
                    buf[ num ] = '\0';
                    fprintf( stderr, "%s", buf );
                    gdsErrorMessage( buf );
                }
            }
        }
        else
        {
            printf( "select failed " );
        }
    }
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: terminateChildProcess			*/
/*                                                         		*/
/* Procedure Description: terminates the child process when parent	*/
/* terminates.								*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
/* Child process id */
static int pid = 0;
static void
terminateChildProcess( void )
{
    struct timespec delay = { 0, 300000000L }; /* 300ms */

    if ( pid != 0 )
    {
        /* this wait is needed to make sure all messages have been sent */
        nanosleep( &delay, NULL );

        /* terminate child process */
        kill( pid, SIGKILL );
        pid = 0;
    }
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsCopyStdToConsole				*/
/*                                                         		*/
/* Procedure Description: copies stdout and stdeerr to the		*/
/* system consoles.							*/
/*                                                         		*/
/* Procedure Arguments: int stdOut, int stdErr				*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int
gdsCopyStdToConsole( int stdOut, int stdErr )
{
    int pdOut[ 2 ] = { 0, 0 }; /* pipe descriptor for stdout */
    int pdErr[ 2 ] = { 0, 0 }; /* pipe descriptor for stderr */

    /* test wheter anything is to do */
    if ( ( stdOut == 0 ) && ( stdErr == 0 ) )
    {
        return 0;
    }

    {
        /* only do it once */
        if ( pid != 0 )
        {
            return 0;
        }

        /* create pipes */
        if ( ( stdOut != 0 ) && ( pipe( pdOut ) == -1 ) )
        {
            return -1;
        }
        if ( ( stdErr != 0 ) && ( pipe( pdErr ) == -1 ) )
        {
            if ( stdOut != 0 )
            {
                close( pdOut[ 0 ] );
                close( pdOut[ 1 ] );
            }
            return -1;
        }

        /* create child process which does the copy */
        if ( ( pid = fork( ) ) == 0 )
        {
            /* child process, never to return */
            waitForMessages( stdOut, stdErr, pdOut[ 0 ], pdErr[ 0 ] );
        }

        else if ( pid == -1 )
        { /* error */
            pid = 0;
            if ( stdOut != 0 )
            {
                close( pdOut[ 0 ] );
                close( pdOut[ 1 ] );
            }
            if ( stdErr != 0 )
            {
                close( pdErr[ 0 ] );
                close( pdErr[ 1 ] );
            }
            return -1;
        }
        else
        { /* parent process */
            /* register exit function which terminates child */
            atexit( terminateChildProcess );

            /* dup std out and err to pipe id */
            if ( stdOut != 0 )
            {
                dup2( pdOut[ 1 ], 1 ); /* do not test for failure */
            }
            if ( stdErr != 0 )
            {
                dup2( pdErr[ 1 ], 2 );
            }
        }
    }

    return 0;
}
